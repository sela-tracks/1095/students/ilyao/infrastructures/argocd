## Install ArgoCD Using Helm
### Source 1: [Installing ArgoCD Using Helm](https://blog.fourninecloud.com/installing-argo-cd-using-helm-ed4a0cd0845a)
### Source 2: [Install ArgoCD instance on Kubernetes](https://github.com/yorammi/ez-cicd-learning/blob/master/learning-workspace-setup/argocd-installation.md)
1.  Add the ArgoCD Helm repository and update it:
```shell
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
```
2.  Install ArgoCD:
```shell
helm install argocd argo/argo-cd -n argocd
```
3.  Run one of the following commands, depending on your shell (Linux or Windows) to change the service type to LoadBalancer:
- For Linux shell: 
```shell
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```
- For Windows shell:
```shell
kubectl patch svc argocd-server -n argocd -p '{\"spec\": {\"type\": \"LoadBalancer\"}}'
```
4. Use the following command in order to get the initial admin password to argoCD:
```
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath='{.data.password}' | base64 -d
```
Now, you are able to enter the ArgoCD platform with the initial admin credentials.

## Setup Continues Delivery
To make a Continues Delivery pipe follow these steps:
1. On ArgoCD go to ``Settings -> CONNECT REPO``
Configure your repository, make sure after you click on connect it doesn't throw an 'failed' status.
2. Go to ``Application -> NEW APP``
Make sure you have ``namespace`` as ``'default' ``
And ```Revision``` as your ``repository's Branch``.

## Uninstall ArgoCD using helm
use the following command to uninstall argocd:
```
helm uninstall argocd -n argocd
```
